import java.sql.*;
import java.io.IOException;

public class ControllersManagement implements IControllersManagement {

	private Database DB;
	
	public ControllersManagement (Database DB){
		this.DB = DB;
	}
	
	public void addController() throws IOException{
		try{
			PreparedStatement sentencia = this.DB.getPreparedStatement("INSERT INTO `Controllers_Table` (CONTROLLER_ID, NAME) VALUES (?, ?)");
			String controller_id = Utils.pedirDato("Introduce el identificador del controlador");
			String name = Utils.pedirDato("Introduce el nombre del controlador:");
			sentencia.setString(1, controller_id);
			sentencia.setString(2, name);
			int resul = sentencia.executeUpdate();
			if (resul == 1)
					System.out.println("Controlador dado de alta satisfactoriamente");
			else 
					System.out.println("Fallo desconocido al dar de alta al controlador");
			sentencia.close();
		}
		catch (SQLException e){
			System.out.println("Ya hay un controlador con ese id. ERROR: "+e);
		}
	}


	public void deleteController() throws IOException{
		try {
			String controller_id = Utils.pedirDato("Introduce el id del controlador a eliminar");
			Statement sentencia = this.DB.getStatementBorrado();
			String sql = "DELETE FROM Controllers_Table WHERE CONTROLLER_ID='"+controller_id+"'";
			int borrados = sentencia.executeUpdate(sql);
			System.out.println(borrados+ "registros borrados");
			sentencia.close();
		}
		catch (SQLException e){
			System.out.println(e);
		}
	}

	
	public void addControllerToUser() {
		/*try {
			
		}
		catch (SQLException e){
			System.out.println(e);
		}*/
		// Primero mirar si el controlador se encuentra en la base de datos
		// 2. Rescatar usuarios y modificar el user_controllers a�adiendo al String un espacio mas la nueva ip.
	}


	public void listControllers() {
		try{
			String controller_id = "";
			String name = "";
			int cont = 0;
			Statement sentencia = this.DB.getStatement();
			ResultSet controllers = sentencia.executeQuery("SELECT * FROM Controllers_Table");
			System.out.println("\nCONTROLADORES EN LA BASE DE DATOS:\n");
			while(controllers.next()){
				System.out.println(cont++);
				controller_id = controllers.getString("CONTROLLER_ID");
				name = controllers.getString("NAME");
				System.out.println("CONTROLLER:  "+controller_id+" / "+name);
				cont++;
			}
			controllers.close();
			sentencia.close();
		}
		catch (SQLException e){
			System.out.println(e);
		}
	}

}
