import com.mecatronica.common.AuthSession;
import com.mecatronica.common.NetworkFunction;
import com.mecatronica.common.NetworkObject;


public class PruebaSendWifi {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Wifi wifi = new Wifi ();
		NetworkObject networkObject = wifi.receive();
		AuthSession authSession = getAuthSession (networkObject);
		if (authSession == null)
			System.out.println("Objeto recibido desconocido");
		else {
			System.out.println("Objeto authSession recibido correctamente");
		}
	}

	public static AuthSession getAuthSession(NetworkObject networkObject) {
		if (networkObject.getIdFunction().equals(NetworkFunction.DBSERVER_AUTHENTICATE_USER)){
			return (AuthSession)networkObject.getObject();
		}
		return null;
	}
}
