import com.mecatronica.common.NetworkObject;


public interface IConnection {

    boolean connect (String identifier);

    void send (NetworkObject data);

    NetworkObject receive ();

    boolean close();
}