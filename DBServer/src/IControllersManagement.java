import java.io.IOException;


public interface IControllersManagement {

	public void addController()  throws IOException;
	
	public void deleteController() throws IOException;
	
	public void addControllerToUser();
	
	public void listControllers();
	
}
