
public class Controller {

	private String controller_Name;
	private int controller_id;
	
    public String getController_Name() {
        return controller_Name;
    }
   
    public void setController_Name(String name) {
        this.controller_Name = name;
    }

    public int getController_id(){
    	return controller_id;
    }
    
    public void setController_id(int id) {
        this.controller_id = id;
    }
}
