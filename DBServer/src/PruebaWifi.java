import java.io.IOException;


//Envio hacia el arduino

public class PruebaWifi {

	public static void main(String[] args) throws IOException, InterruptedException{
		System.out.println("Prueba wifi");
		Wifi wifi = new Wifi ();
		boolean resul = false;
		char respuesta;
		do {
		respuesta = Utils.pedirDato("Introduce 0/1").charAt(0);
		if (respuesta == '1')
			resul = true;
		else resul = false;
		wifi.send(resul);
		System.out.println("Enviado");
		} while(respuesta != '2');
		wifi.close();
	}
}