import java.io.IOException;
import java.sql.SQLException;


public interface IUsersManagement {
	
	public void addUser() throws IOException;
	
	public void modUser();
	
	public void deleteUser() throws IOException;
	
	public User searchUser(int user_id) throws SQLException;

	public boolean checkControllerOfUser (String user_controllers, String c_id);
	
	public void listUsers() throws SQLException;
}
