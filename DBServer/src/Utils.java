import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;


public class Utils {
	/*Se envia a esta funci�n (pedirDato) un string con un mensaje que muestra en pantalla y devuelve un string con los datos obtenidos del teclado. Usar Integer.parseint en la funci�n que llame si se pide un entero*/
	static String pedirDato (String mensaje) throws IOException{ 
	     BufferedReader linea=new BufferedReader (new InputStreamReader (System.in));
	     System.out.println(mensaje);
	     return linea.readLine();
	}
	
	//Acceso a la base de datos
	static Database login () throws IOException, ClassNotFoundException, SQLException{
		String nombreBD = pedirDato("Nombre de la base de datos");
		char respuesta;
		do{
			respuesta = pedirDato("�La base de datos tiene contrase�a?").charAt(0);
		} while (respuesta !='N' && respuesta !='n' && respuesta !='S' && respuesta !='s');
		switch(respuesta){
			case 'N':
			case 'n': return new Database(nombreBD); 
			case 'S':
			case 's': String user = pedirDato ("Introduce nombre de usuario: ");
				String pass = pedirDato ("Introduce contrase�a: ");
				return new Database(nombreBD, user, pass);
			default: System.out.println("ERROR DESCONOCIDO"); return null;
		}
	}
}
