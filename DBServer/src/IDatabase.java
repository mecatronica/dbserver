import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;


public interface IDatabase {

	void close();

	void construirConexion();
	
	public Statement getStatement() throws SQLException;
	
	public Statement getStatementBorrado() throws SQLException;
	
	public PreparedStatement getPreparedStatement(String SQL);
		
}