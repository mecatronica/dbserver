import java.sql.*;

public class Database {
	
	protected String url;
	protected Connection conexion;
	protected Statement sentenciaSQL = null;
	protected String user = null;
	protected String pass = null;
	protected PreparedStatement preparedStatement = null;
	
	public Database (String BD) throws ClassNotFoundException, SQLException{
		Class.forName("com.mysql.jdbc.Driver");
		url = "jdbc:mysql://localhost/" +BD;
		construirConexion();
	}
	
	public Database (String BD, String user, String pass) throws ClassNotFoundException, SQLException{
		Class.forName("com.mysql.jdbc.Driver");
		url = "jdbc:mysql://localhost/" +BD + "?user=" + user + "&password=" + pass;
		construirConexion();
	}
	
	void close() throws SQLException{
		conexion.close();
		//sentenciaSQL.close();
	}
	
	protected void construirConexion(){
		try{
			conexion = DriverManager.getConnection(url);
		}
		catch (SQLException e){
			System.out.println("Error de acceso a la base de datos (nombre de la BD o contraseŠa incorrecta).\nError obtenido: " +e);
		}
	}
	
	public Statement getStatement() throws SQLException{
		sentenciaSQL = conexion.createStatement();
		return sentenciaSQL;
	}
	
	public Statement getStatementBorrado () throws SQLException{
		sentenciaSQL = conexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
		return sentenciaSQL;
	}
	
	public PreparedStatement getPreparedStatement(String SQL) throws SQLException{
		preparedStatement = conexion.prepareStatement(SQL);
		return preparedStatement;
	}
}