package com.mecatronica.common;


public enum NetworkFunction {
	
	DBSERVER_AUTHENTICATE_USER,
	
	DBCONTROLLER_SEND_USER_DATA,

    DBDEVICE_SERVER_RESPONSE,

    DBDEBUGAPP_WIFI_TEST;
	
}
