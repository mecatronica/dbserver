
import gnu.io.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.io.ObjectInputStream;

import com.mecatronica.common.NetworkObject;

public class USBArduino implements IConnection {

	private OutputStream output = null;
	private InputStream input = null;
	
	SerialPort serialPort;
	private final String PORT_NAME = "/dev/tty.usbserial-A9007LNM";
	/** Milliseconds to block while waiting for port open */
	private static final int TIME_OUT = 2000;
	/** Default bits per second for COM port. */
	private static final int DATA_RATE = 9600;

	
	//Constructor vacio
	public USBArduino (){
		boolean conexion = connect(PORT_NAME);
		if (conexion) 
			System.out.println("Conexion USB realizada correctamente");
		else 
			System.out.println("Error en la Conexion USB");
	}

	
	public void send(Boolean resul){
		String envio = "0";
		if (resul)
			envio = "1";
		try {
			output.write(envio.getBytes());
		} catch (IOException e) {
			System.out.println("Error sending data");
		}
	}

	
	public void send(NetworkObject data) {
		// TODO Auto-generated method stub
	}

	
	public NetworkObject receive(){
		NetworkObject resul = new NetworkObject ();
		try {
			ObjectInputStream inputStream = new ObjectInputStream (input);
			resul = (NetworkObject) inputStream.readObject();
		} catch (IOException e) {
			System.out.println("Error receiving data");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return resul;
	}
	
	
	public byte[] receive1 () {
		byte [] recib = null;
		try {
			input.read(recib);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return recib;
	}
	//*/
	
	public boolean connect(String port) {
		CommPortIdentifier portId = null;
		Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();
		// iterate through, looking for the port
		while (portEnum.hasMoreElements()) {
			CommPortIdentifier currPortId = (CommPortIdentifier)portEnum.nextElement();
			System.out.println(currPortId.getName());
			if (PORT_NAME.equals(currPortId.getName())) {
				portId = currPortId;
				break;
			}
		}
		if (portId == null) {
			System.out.println("Could not find COM port.");
			return false;
		}
		try {
			// open serial port, and use class name for the appName.
			serialPort = (SerialPort) portId.open(this.getClass().getName(), TIME_OUT);
			// set port parameters
			serialPort.setSerialPortParams(DATA_RATE, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
			// open the streams
			output = serialPort.getOutputStream();

			input = serialPort.getInputStream();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return true;
	}
	
	
	public boolean close() {
		try {
			output.close();
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
}