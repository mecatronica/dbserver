
/**
 * Created by Ser on 22/11/14.
**/

public class User {

    private String user_Name;
    private String user_Key; //First authentication factor.
    private String user_Pass; //Second authentication factor.
    private int user_id;
    private String user_controllers;

    //Constructor con user_id, name y pass
    public User(int user_id, String name, String key, String pass) {
        this.user_id = user_id;
    	this.user_Name = name;
        this.user_Key = key;
        this.user_Pass = pass;
    }
    
    //Constructor con user_id
    public User(int user_id){
    	this.user_id = user_id;
    }
    
    public String getName() {
        return user_Name;
    }

    public void setName(String name) {
        this.user_Name = name;
    }
    
    
    public String getKey() {
        return user_Key;
    }

    public void setKey(String key) {
        this.user_Key = key;
    }
       
    public String getPass() {
        return user_Pass;
    }

    public void setPass(String pass) {
        this.user_Pass = pass;
    }

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getUser_controllers() {
		return user_controllers;
	}

	public void setUser_controllers(String user_controllers) {
		this.user_controllers = user_controllers;
	}
}