import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

import com.mecatronica.common.AuthSession;
import com.mecatronica.common.NetworkObject;

public class Wifi implements IWifi{
	
	private final int puerto = 2000;
	private final String ip = "192.168.43.166";
	private Socket socket;
	private ServerSocket serverSocket;
	
	//Constructor Vacio
	public Wifi () {
		try {
			serverSocket = new ServerSocket(puerto);
			socket = serverSocket.accept();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//boolean conexion = connect(ip);
		/*if (conexion) 
			System.out.println("Conexion Wifi realizada correctamente");
		else 
			System.out.println("Error en la Conexion Wifi");*/
	}
	
	public String getIp(){
		return ip;
	}
	
	public boolean connect(String ip) {
		/*try {
			socket = new Socket(ip, puerto);
			return true;
		} catch (UnknownHostException e) {
			return false;
		} catch (IOException e) {
			return false;
		}
		*/
		return true;
	}
	
	public void send(Boolean resul) throws UnknownHostException, IOException, InterruptedException {
		//socket = new Socket(ip, puerto);
		OutputStream output = null;
		String envio = "4";
		if (resul)
			envio = "3";
		try {
			output = socket.getOutputStream();
			output.write(envio.getBytes());
		} catch (IOException e) {
			System.out.println("Error sending data");
		}
		/*try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}*/
	}
	
	
	public void send(NetworkObject data) {
		try {
			socket = new Socket(ip, puerto);
			ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
			outputStream.writeObject(data);
		} catch (IOException e) {
			System.out.println("ERROR: IOEXCEPTION DURANTE EL ENVIO DE DATOS: " + e.getMessage());
		}
		
		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public NetworkObject receive() {
		System.out.println("Esperando conexiones...");
		try {
			//serverSocket = new ServerSocket(puerto);
			//socket = serverSocket.accept();
			InputStream inStream = null;
			System.out.println("Conectado!");
			inStream = socket.getInputStream();
			
			//Primero recibe el tama�o de los datos a recibir, para reservar espacio en memoria.
			int hi = inStream.read();
			int lo = inStream.read();
			int length = hi;
			length = length << 8;
			length |= lo;
			byte [] data = new byte[length];
			
			//Sabiendo el tama�o de los datos, serializamos el NetworkObject
			inStream.read(data);
			System.out.println(new String(data));
			NetworkObject nObject = NetworkObject.bytesToObject(data);	
			AuthSession authSession = (AuthSession) nObject.getObject();
			//no.setObject(authSession);
			String ip = socket.getInetAddress().toString();
			authSession.setController_id(ip.substring(1, ip.length()));
			//socket.close();
			//serverSocket.close();
			return nObject;
		} catch (IOException e) {
			System.out.println("ERROR: IOEXCEPTION DURANTE LA RECEPCION DE DATOS: " + e.getMessage());
			return null;
		}
	}


	public boolean close() {
		try {
			serverSocket.close();
			socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

}
