import java.sql.*;
import java.io.IOException;

public class UsersManagement implements IUsersManagement{

	private Database DB;
	
	public UsersManagement (Database DB){
		this.DB = DB;
	}
	
	// A�ade un usuario a la base de datos pidiendo por pantalla los datos
	public void addUser() throws IOException{
		try{
			PreparedStatement sentencia = this.DB.getPreparedStatement("INSERT INTO `Users_Table` (USER_ID, USER_NAME, USER_KEY, USER_PASS, USER_CONTROLLERS) VALUES (?, ?, ?, ?, ?)");
			String user_id = Utils.pedirDato("Introduce el identificador �nico de usuario");
			String name = Utils.pedirDato("Introduce el nombre del usuario:");
			String key = Utils.pedirDato("Introduce la llave del usuario");
			String pass = Utils.pedirDato("Introduce la contrase�a del usuario");
			String user_controllers = Utils.pedirDato("Introduce alg�n controlador al que dicho usuario tenga acceso (separados por espacios)");
			sentencia.setString(1, user_id);
			sentencia.setString(2, name);
			sentencia.setString(3, key);
			sentencia.setString(4, pass);
			sentencia.setString(5, user_controllers);
			int resul = sentencia.executeUpdate();
			if (resul == 1)
					System.out.println("Usuario dado de alta satisfactoriamente");
			else 
					System.out.println("Fallo desconocido al dar de alta el cliente");
			sentencia.close();
		}
		catch (SQLException e) {
			System.out.println("Ya hay un usuario con ese identificador. ERROR: "+e);
		}
	}
	
	
	public void modUser() {
		//TODO
	}
	

	// Elimina un usuario de la base de datos seg�n el identificador dado por teclado
	public void deleteUser() throws IOException {
		try {
			String user_id = Utils.pedirDato("Introduce el id del usuario a eliminar");
			Statement sentencia = this.DB.getStatementBorrado();
			String sql = "DELETE FROM Users_Table WHERE USER_ID='"+user_id+"'";
			int borrados = sentencia.executeUpdate(sql);
			System.out.println(borrados+" registros borrados");
			sentencia.close();
		}
		catch (SQLException e){
		      System.out.println(e);
		}
	}
	
	
	// Busca un usuario en la base de datos y devuelve un objeto User con todo lo rescatado de la BD 
	// o null en caso de no haber ningun usuario con ese user_id en la BD
	public User searchUser(int user_id) throws SQLException{
		User resul_User = new User(user_id);
		Statement sentencia = this.DB.getStatement();
		String sql = "SELECT * FROM `Users_Table` WHERE USER_ID='" + user_id +"'";
		ResultSet user_RS = sentencia.executeQuery(sql);
		if (user_RS.next()){
			resul_User.setName(user_RS.getString("USER_NAME"));
			resul_User.setKey(user_RS.getString("USER_KEY"));
			resul_User.setPass(user_RS.getString("USER_PASS"));
			resul_User.setUser_controllers(user_RS.getString("USER_CONTROLLERS"));
			//return resul_User;
		}
		else {
			resul_User = null;
		}
		user_RS.close();
		sentencia.close();
		return resul_User;
	}
	
	
	// Comprueba si el usuario con el u_id pasado tiene acceso al controlador con c_id pasado
	public boolean checkControllerOfUser (String user_controllers, String c_id) {
		boolean encontrado = false;
		int i = 0;
		// Primero pasar de String a array
		String [] controladores = user_controllers.split(" ");
		// Recorrer array e ir comparando y si es igual encontrado=true
		while (!encontrado && i<controladores.length){
			if (controladores[i].equals(c_id))
				encontrado = true;
			i++;
		}
		if (encontrado){
			System.out.println("usuario tiene acceso");
		}
		else {
			System.out.println("usuario NO tiene acceso");
		}
		return encontrado;
	}
	
	
	// Lista todos los usuarios que se encuentran en la base de datos
	public void listUsers() {
		try{
			int user_id = 0;
			String name, key, pass, user_controllers = "";
			int cont = 0;
			Statement sentencia = this.DB.getStatement();
			ResultSet users = sentencia.executeQuery("SELECT * FROM `Users_Table`");
			System.out.println("\nUSUARIOS EN LA BASE DE DATOS:\n");
			while(users.next()){
				//System.out.println(cont++);
				user_id = users.getInt("USER_ID");
				name = users.getString("USER_NAME");
				key = users.getString("USER_KEY");
				pass = users.getString("USER_PASS");
				user_controllers = users.getString("USER_CONTROLLERS");
				System.out.println(cont++ + ":"+user_id+" / "+name+" / "+key+" / "+pass+" /ACCESO A:  "+user_controllers);
			}
			users.close();
			sentencia.close();
		}
		catch (SQLException e) {
			System.out.println(e);
		}
		
	}
}