import java.io.IOException;
import java.net.UnknownHostException;
import java.sql.SQLException;

import com.mecatronica.common.AuthSession;
import com.mecatronica.common.NetworkFunction;
import com.mecatronica.common.NetworkObject;


public class Server {
	

	public static void main (String [] args) throws IOException, ClassNotFoundException, SQLException{
		Database DB = null;
		do {
			DB = Utils.login();
		} while (DB.conexion == null);
		char respuesta;
		do {
			UsersManagement usersManagement = new UsersManagement(DB);
			ControllersManagement controllersManagement = new ControllersManagement(DB);
			showMainMenu();
			respuesta = Utils.pedirDato("\n1. Gestionar base de datos\n2. Poner servidor en marcha\n\n0. Salir").charAt(0);
			switch(respuesta){
				case '1': menuDataBase(usersManagement, controllersManagement); break;
				case '2': serverON(usersManagement, controllersManagement); break;
			}
		} while (respuesta != '0');
		// Respuesta '0', procedemos a cerrar la BD
		DB.close();
	}

	
	
	//Check if user can open the door and return the result:
	   //Rescato fila del user_id
	   //Compruebo que user tiene acceso a dicho controlador  (user_id tenga relacion con controller_id)
	   //Compruebo que los campos que me han llegado (user_key, user_pass) son iguales a lo de la table
	public static boolean login (AuthSession authSession, UsersManagement usersManagement, ControllersManagement controllersManagement) throws SQLException { 
		// TODO: Hacer que esta funcion siempre dure lo mismo (Cosas de Seguridad de Juanjo)	
		User usuarioDB = usersManagement.searchUser(authSession.getUser_id());
		if (usuarioDB == null){
			System.out.println("No hay un usuario con dicho identificador en la base de datos");
			return false;
		}
		if (!usuarioDB.getKey().equals(authSession.getUser_key())){
			System.out.println("La llave recibida no coincide con la de la base de datos");
			return false;
		}
		if (!usuarioDB.getPass().equals(authSession.getUser_pass())){
			System.out.println("La pass recibida no coincide con la de la base de datos");
			return false;
		}
		if (!usersManagement.checkControllerOfUser(usuarioDB.getUser_controllers(), authSession.getController_id())){
			System.out.println("El usuario no tiene acceso a dicho controlador");
			return false;
		}
		String uname = usuarioDB.getName();
		System.out.println("El usuario con nombre " + uname + " tiene acceso a la base de datos");
		return true; 
	}

	
	public static void showMainMenu () {
		System.out.println("---------------------------------------------------------");
		System.out.println("CONTROLADOR DE ACCESO");
		System.out.println("---------------------------------------------------------");
	}
	
	
	public static void menuDataBase (UsersManagement usersManagement, ControllersManagement controllersManagement) throws IOException, SQLException{
		char respuesta;
		System.out.println("///////// DATABASE MANAGEMENT: /////////");
		do {
			respuesta = Utils.pedirDato("\n1. Add User\n2. Delete User\n3. Modify User\n4. Add Controller\n5. Delete Controller\n6. Add controller to user\n7. List Users\n8. List Controllers").charAt(0);
			switch(respuesta){
				case '1': usersManagement.addUser(); break;
				case '2': usersManagement.deleteUser(); break;
				case '3': usersManagement.modUser(); break;
				case '4': controllersManagement.addController(); break;
				case '5': controllersManagement.deleteController(); break;
				case '6': controllersManagement.addControllerToUser(); break;
				case '7': usersManagement.listUsers(); break;
				case '8': controllersManagement.listControllers(); break;	
			}
		} while(respuesta!=0);
	}
	
	
	public static void serverON (UsersManagement usersManagement, ControllersManagement controllersManagement) throws SQLException{
		boolean respuestaFinal = false;
		System.out.println("///////// �SERVER ON! /////////");
		//USBArduino connection = new USBArduino ();
		Wifi connection = new Wifi();
		while(!respuestaFinal) {
			NetworkObject networkObject = connection.receive();
			AuthSession authSession = getAuthSession (networkObject);
			if (authSession == null)
				System.out.println("Objeto recibido desconocido");
			else {
				// Si se ha recibido correctamente, se pasa a realizar el login
				System.out.println("Objeto authSession recibido correctamente");
				respuestaFinal = login(authSession, usersManagement, controllersManagement);
			}
			try {
				connection.send(respuestaFinal);
			} catch (UnknownHostException e) {
				
				e.printStackTrace();
			} catch (IOException e) {
			
				e.printStackTrace();
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
		}
		
		connection.close();
	}

	
	public static AuthSession getAuthSession(NetworkObject networkObject) {
		if (networkObject.getIdFunction().equals(NetworkFunction.DBSERVER_AUTHENTICATE_USER)){
			return (AuthSession)networkObject.getObject();
		}
		return null;
	}
}
